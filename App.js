import React from 'react';
import {
  NativeModules,
  LayoutAnimation,
  Text,
  TouchableOpacity,
  StyleSheet,
  View,
  Dimensions,
  ImageBackground,
  Button,
} from 'react-native';
import Animated,{Easing, Extrapolate} from 'react-native-reanimated'
import {TapGestureHandler, State} from 'react-native-gesture-handler'
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import ModbusMasterPerformance from './ModbusMasterPerformance'


const { UIManager } = NativeModules;

UIManager.setLayoutAnimationEnabledExperimental &&
  UIManager.setLayoutAnimationEnabledExperimental(true);

const {width, height} = Dimensions.get('window');
const {Value,event,block,cond,eq,set,Clock,startClock,stopClock,debug,timing,clockRunning,interpolate,extrapolate} = Animated;

function runTiming(clock, value, dest) {
  const state = {
    finished: new Value(0),
    position: new Value(0),
    frameTime: new Value(0),
    time: new Value(0), 
  };

  const config = {
    toValue: new Value(0),
    duration: 3000,
    easing: Easing.inOut(Easing.ease),
  };

  return ([
    cond(clockRunning(clock), 0, [
      set(state.finished, 0),
      set(state.frameTime, 0),
      set(state.time, 0),
      set(state.position, value),
      set(config.toValue, dest),
      startClock(clock),
    ]),
    timing(clock, state, config),
    cond(state.finished, debug('stop clock', stopClock(clock))),
    state.position,
  ]);
}
function HomeScreen({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Button
        onPress={() => navigation.navigate('Notifications')}
        title="Go to notifications"
      />
    </View>
  );
}

function NotificationsScreen({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Button onPress={() => navigation.goBack()} title="Go back home" />
    </View>
  );
}
const image = require("./asset/bg.jpg");
const Drawer = createDrawerNavigator();

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      screenInit: true,
    }
    this.textOpacity = new Animated.Value(0);
    this.textMoveX = new Animated.Value(0);
    this.imageBgOpacity = new Animated.Value(0);

    this.text1X = interpolate(this.textMoveX,{
      inputRange:[0,1],
      outputRange:[0,width],
      extrapolate:Extrapolate.CLAMP
    })
    this.text1Y = interpolate(this.textOpacity,{
      inputRange:[0,1],
      outputRange:[0,height/4],
      extrapolate:Extrapolate.CLAMP
    })

    this.text2X = interpolate(this.textMoveX,{
      inputRange:[0,1],
      outputRange:[0,-width],
      extrapolate:Extrapolate.CLAMP
    })
    this.text2Y = interpolate(this.textOpacity,{
      inputRange:[0,1], 
      outputRange:[(height - (height/5)),height/4],
      extrapolate:Extrapolate.CLAMP
    })
  }
  componentDidMount() {
    console.disableYellowBox = true;
    this.spin();
    this.timer = setTimeout(
        () => this.hiddenInitScreen(),
        2500,
    );
  }
  hiddenInitScreen(){
    this.textMoveX.setValue(0)
    Animated.timing(
      this.textMoveX,
      {
        toValue: 1,
        duration: 300,
        easing: Easing.linear
      }
    ).start(() => this.exitScreenInit())

    Animated.timing(
      this.imageBgOpacity,
      {
        toValue: 0,
        duration: 800,
        easing: Easing.linear
      }
    ).start()
  }
  spin() {
    this.textOpacity.setValue(0)
    Animated.timing(
      this.textOpacity,
      {
        toValue: 1,
        duration: 800,
        easing: Easing.linear
      }
    ).start()
    Animated.timing(
      this.imageBgOpacity,
      {
        toValue: 0.8,
        duration: 800,
        easing: Easing.linear
      }
    ).start()
  }
  exitScreenInit(){
    this.setState({screenInit: false});
  }
  
  _renderScreenInit(){    
    return(
      <View style={styles.container}>
        <TapGestureHandler>
          <Animated.View style={{opacity: this.textOpacity,transform:[{translateX:this.text1X,translateY:this.text1Y}]}}>
            <Text style={styles.text1}>Hi!!!</Text>
          </Animated.View>
        </TapGestureHandler>
        <Animated.View style={{opacity: this.textOpacity,transform:[{translateX:this.text2X,translateY:this.text2Y}]}}>
          <Text style={styles.text2}>Uart Debug App</Text>
        </Animated.View>        
      </View>
    )
  }

  render() {    
    if(this.state.screenInit){
      return(
        <View style={styles.container}>
          <Animated.View style={{...StyleSheet.absoluteFill, opacity: this.imageBgOpacity}}>
            <ImageBackground source={image} style={styles.image}>

            </ImageBackground>
          </Animated.View>
          <TapGestureHandler>
            <Animated.View style={{opacity: this.textOpacity,transform:[{translateX:this.text1X,translateY:this.text1Y}]}}>
              <Text style={styles.text1}>Hi!!!</Text>
            </Animated.View>
          </TapGestureHandler>
          <Animated.View style={{opacity: this.textOpacity,transform:[{translateX:this.text2X,translateY:this.text2Y}]}}>
            <Text style={styles.text2}>Uart Debug App</Text>
          </Animated.View>        
        </View>
      )
    }
    else{
      return(
        <NavigationContainer>
          <Drawer.Navigator initialRouteName="Home">
            <Drawer.Screen name="Home" component={ModbusMasterPerformance} />
            <Drawer.Screen name="Notifications" component={NotificationsScreen} />
          </Drawer.Navigator>
        </NavigationContainer>
      )
    }
  }
}

const styles = StyleSheet.create({
  container:{
    //flex: 1,
    
    //justifyContent: 'center'    
    position:"relative"
  },
  container1:{
    flex: 1,
    flexDirection: "column"
  },
  image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center",
    height:height,
    width:null,
  },
  text1:{
    //height: height/6,
    fontSize: 50,
    fontWeight: 'bold',
    fontFamily: 'notoserif',
    textAlign: 'center',
    marginBottom: height/20
  },
  text2:{
    height: height/5,    
    fontSize: 55,
    fontWeight: 'bold',
    color:"red",
    fontFamily: 'lucida grande',
    textAlign: 'center',
    bottom: 0
  }
});
