import React, { Component } from 'react';
import { View, Text, StyleSheet,Dimensions,TouchableOpacity,ImageBackground } from 'react-native';
import Animated,{Easing, Extrapolate} from 'react-native-reanimated'
import LinearGradient from 'react-native-linear-gradient';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import {
  LineChart,
} from 'react-native-chart-kit'

const image = require("./asset/mbPerformanceBg.jpg");
const {Value,event,block,cond,eq,set,Clock,startClock,stopClock,debug,timing,clockRunning,interpolate,extrapolate} = Animated;
const {width, height} = Dimensions.get('window');
const waveThickness = 6;
const sizeButtonDiv = 3;
const scaleRadiusMaxInside = 2;
const scaleRadiusMaxOutside = 1.9;
class ModbusMasterPerformance extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isRunning: false,
      chartData: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
      btnText: 'Start',
      pause: false,
      stopIntervalDataTest: false,
    };
    this.opacityInitBgScreen = new Animated.Value(0);
    this.opacityInitScreen = new Animated.Value(0);
    this.waveOpacity = new Animated.Value(0);
    this.buttonTransformX = new Animated.Value(0);
    this.buttonTransformY = new Animated.Value(0);
    this.opacityChart = new Animated.Value(0);

    this.incraseRadiusWaveInside = interpolate(this.waveOpacity,{
        inputRange:[0,0.4],
        outputRange:[scaleRadiusMaxInside,1],
        extrapolate:Extrapolate.CLAMP
    });
    this.incraseRadiusWaveOutside = interpolate(this.waveOpacity,{
        inputRange:[0,0.4],
        outputRange:[scaleRadiusMaxOutside,1],
        extrapolate:Extrapolate.CLAMP
    });
    var intervalDemoDataChart;
  }
  componentDidMount() {
      this.InitScreen();
  }
  InitScreen(){
    this.opacityInitScreen.setValue(0)
    Animated.timing(
      this.opacityInitScreen,
      {
        toValue: 1,
        duration: 600,
        easing: Easing.linear
      }
    ).start(()=>{
        this.waveOpacity.setValue(0);
        this.WaveRun();
    })
    this.opacityInitBgScreen.setValue(0)
    Animated.timing(
      this.opacityInitBgScreen,
      {
        toValue: 1,
        duration: 600,
        easing: Easing.linear
      }
    ).start();
    Animated.timing(
        this.waveOpacity,
        {
          toValue: 1,
          duration: 600,
          easing: Easing.linear
        }
    ).start();
  }
  WaveRun(){
    this.waveOpacity.setValue(0.4)
    Animated.timing(
      this.waveOpacity,
      {
        toValue: 0,
        duration: 2000,
        easing: Easing.linear
      }
    ).start(() => this.RestartWaveRun())
    
  }
  RestartWaveRun(){    
    this.timer = setTimeout(
        () => this.WaveRun(),
        2000,
    );
  }
  moveButton(){
    if(!this.state.isRunning){
      this.setState({isRunning: true, btnText: 'Running'});

      this.buttonTransformX.setValue(0);
      Animated.timing(
        this.buttonTransformX,
        {
          toValue: 250,
          duration: 600,
          easing: Easing.linear
        }
      ).start()

      this.buttonTransformY.setValue(0); 
      Animated.timing(
        this.buttonTransformY,
        {
          toValue: 90,
          duration: 600,
          easing: Easing.linear
        }
      ).start();

      this.opacityChart.setValue(0);
      Animated.timing(
        this.opacityChart,
        {
          toValue: 0.97,
          duration: 1000,
          easing: Easing.linear
        }
      ).start();

      intervalDemoDataChart = setInterval(
        () => this.demoDataChart(),
        800,
      );
    }
    else{
      if(!this.state.pause){
        // Change text button
        this.setState({btnText: 'Stop'});

        this.state.pause = true;
        this.state.stopIntervalDataTest = true;
        
        console.log('clear interval')
      }else{
        // Change text button
        this.setState({btnText: 'Running'});

        this.state.pause = false;
        intervalDemoDataChart = setInterval(
          () => this.demoDataChart(),
          800,
        );
      }
    }
  }
  demoDataChart(){
    if(this.state.stopIntervalDataTest){
      this.state.stopIntervalDataTest = false;
      clearInterval(intervalDemoDataChart);
    }
    else{
      var chartDataTemp = this.state.chartData;
      chartDataTemp.push(Math.floor(Math.random() * 10));
      chartDataTemp.shift();
      this.setState({chartData: chartDataTemp});
    }
  }

  _renderChart(){
    if (!this.state.isRunning) {
      return null;
    } else {
      return(
        <LineChart
          data={{                
            datasets: [
              {
                data: this.state.chartData
              }
            ]
          }}
          width={width} // from react-native
          height={height - height/sizeButtonDiv - 70}
          yAxisInterval={1} // optional, defaults to 1
          chartConfig={{
            backgroundColor: "#e26a00",
            backgroundGradientFrom: "#fb8c00",
            backgroundGradientTo: "#ffa726",
            decimalPlaces: 0, // optional, defaults to 2dp
            color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
            labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
            style: {
              borderRadius: 16
            },
            propsForDots: {
              r: "2",
              strokeWidth: "1",
              stroke: "#ffa726"
            }
          }}
          bezier
          style={{
            marginVertical: 8,
            borderRadius: 16
          }}
        />
      )      
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Animated.View style={{opacity: this.opacityInitBgScreen,transform:[{translateX:0,translateY:0}]}}>
            <ImageBackground source={image} style={styles.imageBg}>

            </ImageBackground>
        </Animated.View>

        <Animated.View style={{opacity:this.opacityChart, backgroundColor: 'rgba(52, 52, 52, 0.1)'}}>
          {this._renderChart()}
        </Animated.View>  

        <Animated.View style={{
          opacity:this.opacityChart,
          width: width/1.4,
          height: height/sizeButtonDiv,
          backgroundColor: '#e3fbd9',
          borderRadius: 20,
          marginTop: 12,
        }}>
          <Text style={{
            marginLeft: 40,
            marginTop: 5,
            marginBottom: 5,
            fontSize: 20,
            color: '#5b7358',
          }}>Performance: 87%</Text>
          <Text style={{
            marginLeft: 40,
            marginTop: 5,
            marginBottom: 5,
            fontSize: 20,
            color: '#5b7358',
          }}>Max Time: 10ms</Text>
          <Text style={{
            marginLeft: 40,
            marginTop: 5,
            marginBottom: 5,
            fontSize: 20,
            color: '#5b7358',
          }}>Average Time: 8ms</Text>
        </Animated.View>      
        
        <Animated.View style={{
          position: 'absolute', 
          top: 0, left: 0, 
          right: 0, bottom: 0, 
          justifyContent: 'center', 
          alignItems: 'center',
          opacity:this.waveOpacity,
          width: height/sizeButtonDiv,
          height: height/sizeButtonDiv,
          borderRadius: (height/(sizeButtonDiv*2)),
          backgroundColor: 'red',
          left: width/2 - (height/(sizeButtonDiv*2)),
          top: height/2 - (height/(sizeButtonDiv*2))-(getStatusBarHeight()/2),
          transform:[{translateX:this.buttonTransformX,translateY:this.buttonTransformY,scale:this.incraseRadiusWaveOutside}]
          }} />
        <Animated.View style={{
            opacity: this.opacityInitScreen,
            position: 'absolute', 
            top: 0, left: 0, 
            right: 0, bottom: 0, 
            justifyContent: 'center', 
            alignItems: 'center',
            transform:[{translateX:this.buttonTransformX,translateY:this.buttonTransformY}]
            }}>
              <LinearGradient colors={['#4c669f', '#5b77a1', '#aa5599']} style={{                
                  width:height/sizeButtonDiv,
                  height:height/sizeButtonDiv,
                  borderRadius:height/(sizeButtonDiv*2),
                }}>
                <TouchableOpacity style={{
                    width:height/sizeButtonDiv,
                    height:height/sizeButtonDiv,
                    borderRadius:height/(sizeButtonDiv*2),
                    alignItems:'center',
                    justifyContent:'center'}} onPress={()=>this.moveButton()}>
                    <Text style={styles.btnText}>{this.state.btnText}</Text>            
                </TouchableOpacity>
            </LinearGradient>            
        </Animated.View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
    container:{
        backgroundColor: 'white',
        flex: 1,
    },
    imageBg: {
        flex: 1,
        resizeMode: "cover",
        justifyContent: "center",
        height:height,
        width:null,
      },
    circleOutside: {
        width: height/4,
        height: height/4,
        borderRadius: 100/2,
        backgroundColor: 'red',
        top: height/2 - (height/8) - 100,
        left: width/2 - (100/2),        
    },
    circleInside: {
        width: height/4,
        height: height/4,
        borderRadius: 100/2,
        backgroundColor: 'white',
        top: 0,
        left: width/2 - (100/2),
        transform: [{translateX:100,translateY:10}],        
    },
    btnText:{
        fontSize: height/12,
        color: 'white',
    },    
  });

export default ModbusMasterPerformance;
